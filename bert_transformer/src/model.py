import os 
os.chdir('C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\src')
import torch
import numpy as np
import matplotlib.pyplot as plt
import torch.nn as nn
from torch.optim import AdamW
from torch.utils.data import Dataset, DataLoader
from utils import AverageMeter
from transformers import BertConfig, BertModel, get_linear_schedule_with_warmup
from data import SentimentAnalysisDataset
import time

class BertClassification(nn.Module):
    def __init__(self, backbone, num_class = 3):
        super().__init__()
        self.backbone = backbone
        self.drop = nn.Dropout(p=0.3)
        self.lin = nn.Linear(backbone.config.hidden_size, num_class)
        self.softmax  = nn.Softmax(dim = 1)

    def forward(self, x, attention_mask) : 
        x = self.backbone(x, attention_mask)['pooler_output']
        x = self.drop(x)
        x = self.lin(x)
        x = self.softmax(x)
        return x

def accuracy(outputs, targets) : 
    outputs = torch.argmax(outputs, dim = 1)
    targets = torch.argmax(targets, dim = 1)
    acc = (outputs == targets).float().mean().item()
    return acc

def test(model, dataloader, criterion, device, TestLoss, Accuracy):
    with torch.no_grad():
        for i, (input_ids, attention_mask, targets) in enumerate(dataloader):
            optimizer.zero_grad()
            input_ids = input_ids.to(device)
            attention_mask = attention_mask.to(device)
            targets = targets.to(device)

            outputs = model(input_ids, attention_mask)
            loss = criterion(outputs, targets)
            acc = accuracy(outputs, targets) 
            Accuracy.update(acc)
            TestLoss.update(loss.item())

def train(model, trainloader, testloader, epochs, criterion, device, optimizer, scheduler, TrainLoss, TestLoss, Accuracy, test_at_each_epoch = False):
    model.to(device)
    for epoch in range(epochs) : 
        t = time.time()
        model.train()
        for i, (input_ids, attention_mask, targets) in enumerate(trainloader):
            optimizer.zero_grad()
            input_ids = input_ids.to(device)
            attention_mask = attention_mask.to(device)
            targets = targets.to(device)
            outputs = model(input_ids, attention_mask)

            loss = criterion(outputs, targets)
            loss.backward()
            nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
            optimizer.step()
            scheduler.step()
            TrainLoss.update(loss.item())

        print(f'TRAINING: epoch[{epoch}/{epochs - 1}] time = {time.time() - t:.2f} loss = {TrainLoss.avg:.2f}')
        if test_at_each_epoch : 
            test(model, testloader, criterion, device, TestLoss, Accuracy)
            print(f'TESTING: epoch[{epoch}/{epochs - 1}] loss = {TestLoss.avg:.2f} acc = {Accuracy.avg:.2f}')
            TestLoss.new_epoch()
        TrainLoss.new_epoch()
        Accuracy.new_epoch()

if __name__ == '__main__':
    # Hyperparameters settings
    epochs = 50
    lr =   2e-5
    save = True
    device = torch.device('cuda:0')

    # Define model
    config = BertConfig()
    backbone = BertModel(config).from_pretrained('bert-base-uncased')
    model = BertClassification(backbone)

    # Define dataset, dataloader and criterion
    trainfile = 'C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\data\\traindata.csv' 
    testfile = 'C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\data\\devdata.csv'
    trainset = SentimentAnalysisDataset(trainfile)
    testset = SentimentAnalysisDataset(testfile)

    trainloader = DataLoader(trainset, batch_size=16, num_workers = 4)
    testloader = DataLoader(testset, batch_size=16, num_workers = 4)
    criterion = torch.nn.CrossEntropyLoss() #check the format of the labels

    # Define optimizer and scheduler
    optimizer = AdamW(model.parameters(), lr = lr)
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=epochs*len(trainloader))

    # Utils
    model_name = f'BertModel-epoch={epochs}_lr={lr}'
    checkpoints_dir = 'C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\chekpoints'
    TrainLoss = AverageMeter(name = 'train loss')
    TestLoss = AverageMeter(name = 'test loss')
    Accuracy = AverageMeter(name = 'accuracy')

    train(model, trainloader, testloader, epochs, criterion, device, optimizer, scheduler, TrainLoss, TestLoss, Accuracy)
    if save : 
        torch.save(model.state_dict(), os.path.join(checkpoints_dir, model_name + '.pth'))

    TrainLoss.cross_plot(TestLoss)
    Accuracy.plot()