import matplotlib.pyplot as plt
import numpy as np

class AverageMeter(object):
    """ Computes and stores the average and current value"""
    def __init__(self, name = ''):
        self.reset()
        self.name = name
        self.values = []
    
    def reset(self):
        self.val = 0.
        self.avg = 0.
        self.sum = 0.
        self.count = 0

    def new_epoch(self):
        self.values.append(self.avg)
        self.reset()
    
    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def plot(self) : 
        plt.figure()
        plt.plot(np.arange(len(self.values)), self.values)
        plt.ylabel(self.name)
        plt.xlabel('epoch number')
        plt.show()

    def cross_plot(self, *args) : 
        """[summary] Plot on the same the graph of the variable held in self as well as 
        all the other variables held in AverageMeter objects [*args]

        Input:
            *args: list of AverageMetter objects whose variables should be ploted
        """
        plt.figure()
        legend = []
        plt.plot(np.arange(len(self.values)), self.values)
        legend.append(self.name)
        for obj in args : 
            plt.plot(np.arange(len(obj.values)), obj.values)
            legend.append(obj.name)
        plt.xlabel('epoch number')
        plt.legend(legend)
        plt.show()
