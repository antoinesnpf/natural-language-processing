from __future__ import division
import argparse
import pandas as pd

# useful stuff
import numpy as np
from scipy.special import expit
from sklearn.preprocessing import normalize
from collections import Counter
import pickle


__authors__ = ['author1','author2','author3']
__emails__  = ['fatherchristmoas@northpole.dk','toothfairy@blackforest.no','easterbunny@greenfield.de']

def text2sentences(path):
    # feel free to make a better tokenization/pre-processing
    sentences = []
    unwanted_char = ['|','?','_','!','¶','€',':',']','\t','^',
                 '/','*','}','$','~','&','{','[','(','"',
                 '`','=',';','@','+','%',')','>','.',',',
                 '\\','"','£','\n','-']
    with open(path) as f:
        for l in f:
            for char in unwanted_char : 
                l.replace(char, '')
            
            sentences.append( l.lower().split() )
    return sentences

def loadPairs(path):
    data = pd.read_csv(path, delimiter='\t')
    pairs = zip(data['word1'],data['word2'],data['similarity'])
    return pairs

def one_hot(int, size) : 
    res = np.zeros((size, ))
    res[int] = 1 
    return res

def sigmoid(x) : 
    return 1 / (1 + np.exp(-x))

class SkipGram:
    def __init__(self, 
                sentences, 
                nEmbed=100, 
                negativeRate=5, 
                winSize = 5, 
                minCount = 5, 
                lr = 0.01, 
                epochs = 10, 
                plot_every = 100):

        # Creating vocabulary and count of words
        self.trainset = sentences
        self.vocab = []
        self.count = []
        self.rectified_count = []
        counter = Counter(np.concatenate(sentences)) 
        for word in counter.keys() :
            if counter[word] > minCount : 
                self.vocab.append(word)
                self.count.append(counter[word])
                self.rectified_count.append(counter[word]**(3/4))

        self.negative_sampling_probabilities = np.array(self.rectified_count) / np.sum(self.rectified_count)

        self.w2id = {} 
        for i, word in enumerate(self.vocab) :
            self.w2id[word] = i 
        
        # Creating neural network's structure 
        self.V = len(self.vocab)
        self.E = nEmbed
        self.W1 = np.random.normal(size = (self.E, self.V))
        self.W2 = np.random.normal(size = (self.V, self.E))


        self.nEmbed = nEmbed
        self.negativeRate = negativeRate
        self.winSize = winSize
        self.lr = lr
        self.epochs  = epochs
        self.loss = []
        self.plot_every = plot_every


    def sample(self, omit):
        """samples negative words, ommitting those in set omit"""
        negativeIds = []
        k=0
        while k < self.negativeRate : 
            negative_word = np.random.choice(self.vocab, p = self.negative_sampling_probabilities)
            negative_id = self.w2id[negative_word] 
            if negative_id not in omit : 
                k += 1 
                negativeIds.append(negative_id)

        return negativeIds
        

    def train(self):
        for epoch in range(self.epochs) : 
            for counter, sentence in enumerate(self.trainset):
                current_loss = 0
                trainWords = 0
                
                sentence = list(filter(lambda word: word in self.vocab, sentence)) # ensure every word of sentence is in the vocab

                for wpos, word in enumerate(sentence):
                    wIdx = self.w2id[word]
                    winsize = np.random.randint(self.winSize) + 1
                    start = max(0, wpos - winsize)
                    end = min(wpos + winsize + 1, len(sentence))


                    for context_word in sentence[start:end]:
                        ctxtId = self.w2id[context_word]
                        if ctxtId == wIdx: continue
                        negativeIds = self.sample({wIdx, ctxtId})
                        current_loss +=  self.trainWord(wIdx, ctxtId, negativeIds)
                        trainWords += 1

                if counter % self.plot_every == 0:
                    print(f' > epoch [{epoch}/{self.epochs}] iter [{counter}/{len(self.trainset)}]')
                    print(f'>> loss : {current_loss / trainWords}')
                    self.loss.append(current_loss / trainWords)


    def trainWord(self, wordId, contextId, negativeIds):
        """
            -For each pair of words {mainWord, trainWord}, calculate the loss between the network's output and the true label
            -Calculate the gradient of the loss wrt the weights matrices
            -Updates the weights matrices
        """
        loss = 0 

        input = one_hot(wordId, self.V) # size V
        latent_representation = self.W1 @ input # size E
        output = sigmoid(self.W2 @ latent_representation) #size V
        context_output = output[contextId] # classifier's output for contextWord
        loss += -np.log(context_output)  #binary cross entropy

        temp = sigmoid(-self.W2[contextId] @ latent_representation)
        grad_W1 = -temp * self.W2[contextId]
        grad_W2 = -temp * latent_representation

        self.W1[:, wordId] -= self.lr * grad_W1
        self.W2[contextId] -= self.lr * grad_W2

        for negativeId in negativeIds:
            output = sigmoid(- self.W2 @ latent_representation)
            negative_output = output[negativeId]

            loss +=  -np.log(negative_output)  #binary cross entropy

            temp = sigmoid(self.W2[negativeId] @ latent_representation)
            grad_W1 = temp * self.W2[negativeId]
            grad_W2 = temp * latent_representation
            self.W1[:, wordId] -= self.lr * grad_W1
            self.W2[contextId] -= self.lr * grad_W2

        return loss

    def save(self,path):
        with open(path, 'wb') as f: 
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
        

    def similarity(self,word1,word2):
        """
            computes similiarity between the two words. unknown words are mapped to one common vector
        :param word1:
        :param word2:
        :return: a float \in [0,1] indicating the similarity (the higher the more similar)
        """


        if word1 in self.vocab : 
            e1 = self.W1 @ one_hot(self.w2id[word1], self.V)
            norm1 = np.linalg.norm(e1)
        else : 
            e1 = np.zeros((self.E, ))
            norm1 = 1

        if word2 in self.vocab : 
            e2 = self.W1 @ one_hot(self.w2id[word2], self.V)
            norm2 = np.linalg.norm(e2)
        else : 
            e2 = np.zeros((self.E, ))
            norm2=1

        return   (1 + (e1/norm1) @ (e2/norm2)) /2 

    @staticmethod
    def load(path):
        with open(path, 'rb') as f : 
            return pickle.load(f)

if __name__ == '__main__':
    """
    test = True
    if test : text = 'simlex.csv'
    else : text = 'train.txt'
    model = 'mymodel.model'
    if not test: # train 
        sentences = text2sentences(text)
        sg = SkipGram(sentences)
        sg.train()
        sg.save(model)

    else:
        pairs = loadPairs(text)

        sg = SkipGram.load(model)
        for a,b,_ in pairs:
            # make sure this does not raise any exception, even if a or b are not in sg.vocab
            print(sg.similarity(a,b), _)
    """


    parser = argparse.ArgumentParser()
    parser.add_argument('--text', help='path containing training data', required=True)
    parser.add_argument('--model', help='path to store/read model (when training/testing)', required=True)
    parser.add_argument('--test', help='enters test mode', action='store_true')

    opts = parser.parse_args()

    if not opts.test:
        sentences = text2sentences(opts.text)
        sg = SkipGram(sentences)
        sg.train()
        sg.save(opts.model)

    else:
        pairs = loadPairs(opts.text)

        sg = SkipGram.load(opts.model)
        for a,b,_ in pairs:
            # make sure this does not raise any exception, even if a or b are not in sg.vocab
            print(sg.similarity(a,b))

