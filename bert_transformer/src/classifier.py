import os
from tkinter import E
import torch
from torch.optim import AdamW
from torch.utils.data import Dataset, DataLoader
from transformers import BertConfig, BertModel, get_linear_schedule_with_warmup
from zmq import device

from utils import AverageMeter
from data import SentimentAnalysisDataset
from model import BertClassification, train



class Classifier:
    """The Classifier"""
    def __init__(self, epochs = 50, lr = 2e-5, save = False, plot = False):

        # main hyperparams
        self.epochs = epochs
        self.lr = lr 
        self.save = save
        self.plot = plot

        # utils 
        self.device = torch.device('cuda:0')
        self.checkpoints_dir = 'C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\chekpoints'
        self.model_name = f'BertModel-epoch={self.epochs}_lr={self.lr}'

    #############################################

    def load(self, checkpoint_dir = None, model_name = None) : 

        if checkpoint_dir is None : 
            checkpoint_dir = self.checkpoints_dir       
        if model_name is None : 
            model_name = self.model_name
        
        config = BertConfig()
        backbone = BertModel(config).from_pretrained('bert-base-uncased')
        self.model = BertClassification(backbone)
        self.model.load_state_dict(torch.load(os.path.join(checkpoint_dir, model_name + '.pth')))


    def train(self, trainfile, devfile=None):
        """
        Trains the classifier model on the training set stored in file trainfile
        WARNING: DO NOT USE THE DEV DATA AS TRAINING EXAMPLES, YOU CAN USE THEM ONLY FOR THE OPTIMIZATION
         OF MODEL HYPERPARAMETERS
        """ 

        # Define model
        config = BertConfig()
        backbone = BertModel(config).from_pretrained('bert-base-uncased')
        self.model = BertClassification(backbone)
        self.model.to(self.device)

        # Define dataset, dataloader and criterion
        trainset = SentimentAnalysisDataset(trainfile)
        testset = SentimentAnalysisDataset(devfile)
        trainloader = DataLoader(trainset, batch_size=16, num_workers = 4)
        testloader = DataLoader(testset, batch_size=16, num_workers = 4)
        criterion = torch.nn.CrossEntropyLoss()

        # Define optimizer and scheduler
        optimizer = AdamW(self.model.parameters(), lr = self.lr)
        scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps = self.epochs * len(trainloader))

        # Utils
        TrainLoss = AverageMeter(name = 'train loss')
        TestLoss = AverageMeter(name = 'test loss')
        Accuracy = AverageMeter(name = 'accuracy')

        train(self.model, trainloader, testloader, self.epochs, criterion, self.device, optimizer, scheduler, TrainLoss, TestLoss, Accuracy)
        
        if self.save : 
            torch.save(self.model.state_dict(), os.path.join(self.checkpoints_dir, self.model_name + '.pth'))

        if self.plot : 
            TrainLoss.cross_plot(TestLoss)
            Accuracy.plot()


    def predict(self, datafile):
        """Predicts class labels for the input instances in file 'datafile'
        Returns the list of predicted labels
        """
    
        self.model.to(self.device)

        predictions = []
        testset = SentimentAnalysisDataset(datafile)
        for input_ids, attention_mask, targets in testset : 

            output = self.model(input_ids.unsqueeze(0).to(self.device), attention_mask.unsqueeze(0).to(self.device)).squeeze(0)
            predicted_label = torch.argmax(output, dim = 0).item()

            if predicted_label == 0 : 
                predictions.append('positive')
            elif predicted_label == 1 : 
                predictions.append('neutral')
            elif predicted_label == 2 : 
                predictions.append('negative')
            
        return predictions


if __name__ == '__main__':
    trainfile = 'C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\data\\traindata.csv' 
    testfile = 'C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\data\\devdata.csv'
 
    classifier = Classifier(epochs = 50)
    classifier.load()
    classifier.predict(testfile)
