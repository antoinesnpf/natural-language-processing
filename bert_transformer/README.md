# Opinion analysis

## Students: 
- Axel Demouge
- Antoine Schnepf

# 

The general architecture of the opinion analysis model is as follows: 

Sentence -> Tokenization -> Attention Based Model -> Inferred Opinion

## Preprocessing 
The Tokenization part is considered as a preprocessing of the data. We use the Bert Tokenizer. 
1) The undesirables words and punctuation signs are removed, and the data is lowercased.
2) The vocabulary is constructed. Words can now be represented by an interger between 0 and V-1, where `V` is the vocavulary size. We call this integer the word id.
3) The tokenizer greedily finds the longest sentence of the dataset `S_max`
4) Each sentence is transformed into a vector `[id_1, ..., id_S]` where `S` is the number of words in the sentence
5) Three reserved tokens (`[CLS]`, `[SEP]`, `[PAD]`) are added to the vectorial representation of the sentence. It now looks like `[[CLS], id_1, ..., id_S, [SEP], [PAD], ..., [PAD]]`.
`[CLS]` tokens means that the considered task is classification
`[SEP]` tokens signifies the end of the sentence
`[PAD]` tokens are added to the vectorial representation until the vector has a lenghts `S_max`

## Training
Once the data is preprocessed with the tokenizer, it can be processed by the Bert Attention Based Model. 

The architecture of the model is as follows : 
 - Pretrained Bert Model
 - Dropout
 - Linear Layer
 - Softmax

The model is first trained on the dataset traindata.csv with the tokenizer preprocessing done on the fly. To do so, we use the Cross Entropy loss function. The loss is computed between the output of the model 
and the target (postive, neutral or negative). During backpropagation, we ensure that gradients to not diverge by clipping them so that their norm is at most 1. This
is done according to the paper 'BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding'.
The training lasts for 50 epochs. One epoch epoch corresponds to a loop over the whole dataset.

![image](./graphs/losses.png)

## Testing
We can now test the model. To do so, we use the test split of the data (devdata.csv). We obtain a final accuracy of 0.76.

![image](./graphs/accuracy.png)
