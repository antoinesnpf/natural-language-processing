import numpy as np

# implementation of BLEU 
def get_n_grams(segment, n) : 
    counts = {}
    total = 0
    for i in range(len(segment) - n + 1):
        n_gram = tuple(segment[i:i + n])
        if n_gram in counts.keys() : 
            counts[n_gram] += 1
        else : 
            counts[n_gram] = 1
        total += 1 
    return counts


def get_adapted_precision(counts, counts_true) : 
    'compute adapted precision for infered n-grams and true n-grams'
    total = 0
    for n_gram in counts.keys() : 
        total += counts[n_gram]
    if total == 0 :
        return 0

    precision = 0
    for n_gram in counts.keys() : 

        nb_occurance = counts[n_gram]
        if n_gram in counts_true.keys() : 
            nb_occurance_true = counts_true[n_gram]
        else : 
            nb_occurance_true = 0
        nb_occurance_clipped = min(nb_occurance, nb_occurance_true)
        precision += nb_occurance_clipped

    return precision / total


def BLEU(segment, segment_true, n_max = 4) :
    'compute the BLEU metric with uniform weighting'

    #compute BP factor
    c = len(segment) 
    r = len(segment_true)
    BP = np.exp(1 - r/c) if c <= r else 1 

    # compute geometric mean of n-grams adapted precisions
    geometric_mean = 1
    for n in range(n_max) : 
        counts = get_n_grams(segment, n)
        counts_true = get_n_grams(segment_true, n)
        geometric_mean *= get_adapted_precision(counts, counts_true)

    if geometric_mean == 0 :
        return 0
    else : 
        return BP * np.exp( 1/n_max * np.log(geometric_mean))



# implementation of ROUGE
def get_precision(counts, counts_true) : 
    'compute adapted precision for infered n-grams and true n-grams'
    total = 0
    for n_gram in counts.keys() : 
        total += counts[n_gram]

    if total == 0 :
        return 0 
        
    precision = 0
    for n_gram in counts.keys() : 

        nb_occurance = counts[n_gram]
        if n_gram in counts_true.keys() : 
            precision += nb_occurance 

    return precision / total

def get_recall(counts, counts_true) : 
    total_true = len(list(counts_true.keys())) 

    recall = 0
    for n_gram in counts.keys() : 
        if n_gram in counts_true.keys() : 
            recall += 1 

    return recall / total_true

def ROUGE_N(segment, true_segment, n) : 
    'compute the ROUGE-N metric : recall using n-grams'
    counts = get_n_grams(segment, n)
    counts_true = get_n_grams(true_segment, n)
    precision = get_precision(counts, counts_true)
    recall = get_recall(counts, counts_true)

    denum = precision + recall
    if denum == 0 : 
        return (0, 0, 0)
    f1 = precision * recall / denum
    return precision, recall, f1

def lcs(segment_A, segment_B):
    'compute longuest common sequence using dynamic programming'
    p = len(segment_A)
    q = len(segment_B)
    temp = np.zeros((p+1, q+1))
    for i in range(p + 1):
        for j in range(q + 1):
            if i == 0 or j == 0 :
                temp[i, j] = 0
            elif segment_A[i-1] == segment_B[j-1]:
                temp[i, j] = temp[i-1, j-1]+1
            else:
                temp[i, j] = max(temp[i-1, j], temp[i, j-1])
    return temp[p, q]
    
def ROUGE_L(segment, true_segment) : 
    'compute the ROUGE-L metric : longest common subsequence'
    return lcs(segment, true_segment) / len(true_segment)


# other
def tokenize(sentence) : 

    unwanted_char = ['|','?','_','!','¶','€',':',']','\t','^',
                '/','*','}','$','~','&','{','[','(','"',
                '`','=',';','@','+','%',')','>','.',',',
                '\\','"','£','\n','-']

    for char in unwanted_char:
        sentence = sentence.replace(char, '')

    return sentence.lower().strip().split(' ')


def correl(A, B) : 
    return A/np.linalg.norm(A)  @ B/np.linalg.norm(B)