from torch.utils.data import Dataset, DataLoader
import torch
import numpy as np
import pandas as pd
from transformers import AutoTokenizer,BertTokenizer, TFBertForSequenceClassification
from transformers import InputExample, InputFeatures


class SentimentAnalysisDataset(Dataset):

    def __init__(self, datafile):

        df = pd.read_csv(datafile, sep ='\t', header=None)
        df = df.rename(columns={0: "labels", 1: "aspect_category",2:"aspect_term",3:"time",4:"sentence"})
        self.text = df.sentence.to_numpy()
        self.targets = df.labels.to_numpy()
        self.tokenizer = AutoTokenizer.from_pretrained("bert-base-cased")
    
    def __len__(self):
        return len(self.text)

    def __getitem__(self, idx):
        encoding = self.tokenizer.encode_plus(
                            self.text[idx],
                            max_length=96,
                            add_special_tokens=True,
                            return_token_type_ids=False,
                            padding='max_length',
                            return_attention_mask=True,
                            return_tensors='pt', 
                            truncation = True 
            )
                            
        inputs_ids = encoding['input_ids'].squeeze(0)
        attention_mask = encoding['attention_mask'].squeeze(0)

        if self.targets[idx] == 'positive' : 
            target = torch.Tensor([1, 0, 0])
        elif self.targets[idx] == 'neutral' : 
            target = torch.Tensor([0, 1, 0])
        elif self.targets[idx] == 'negative' : 
            target = torch.Tensor([0, 0, 1])

        return inputs_ids, attention_mask, target

if __name__ == '__main__' :
    trainfile = 'C:\\Users\\Antoine\\Desktop\\GIT\\natural-language-processing\\bert_transformer\\data\\traindata.csv' 
    dataset = SentimentAnalysisDataset(trainfile)
    dataloader = DataLoader(dataset, batch_size=8, shuffle = True)
    it = iter(dataloader)
    inputs_ids, attention_masks, target = it.next()

